# OpenML dataset: Hyperplane

https://www.openml.org/d/43122

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Rotating hyperplane is a stream generator that generates d-dimensional classification problems in which the prediction is defined by a rotating hyperplane. By changing the orientation and position of the hyperplane over time in a smooth manner, we can introduce smooth concept drift. 
This version is created with MOA library with drift parameters t equal to 0.1 (control the magnitude of change after every instance) within a window of 100k. Probability of reversing change direction is fixed at 10%. 
It contains 500k instances with 10 numeric features. 5% noise is added by randomly changing the class labels.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43122) of an [OpenML dataset](https://www.openml.org/d/43122). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43122/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43122/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43122/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

